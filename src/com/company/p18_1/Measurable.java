package com.company.p18_1;

/**
 * Measureable interface to implement
 * @param <T>
 */
public interface Measurable<T> {
    double getMeasure();
}
