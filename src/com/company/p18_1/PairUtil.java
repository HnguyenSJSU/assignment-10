package com.company.p18_1;

/**
 * create a PairUtil class that print Val array
 */
public class PairUtil {
    //val array to return min max pair
    private Val[] temp = new Val[2];
    //private Measurable elements
    private Measurable numOne;
    private Measurable numTwo;

    /**
     * constructor for PairUtil
     * @param arr
     */
    public PairUtil(Val[] arr){
        this.numOne = arr[0];
        this.numTwo = arr[1];
    }

    /**
     * return string from pairutil Object
     * @return
     */
    @Override
    public String toString()
    {
        return String.format("Min and Max value is %.0f, %.1f", (this.numOne.getMeasure()),(this.numTwo.getMeasure()));
    }

    /**
     * setter for numOne
     * @param numOne
     */
    public void setNumOne(Measurable numOne) {
        this.numOne = numOne;
    }

    /**
     * setter fpr numTwo
     * @param numTwo
     */
    public void setNumTwo(Measurable numTwo) {
        this.numTwo = numTwo;
    }

    /**
     * getter for numOne
     * @return
     */
    public Measurable getNumOne() {
        return numOne;
    }

    /**
     * getter for numTwo
     * @return
     */
    public Measurable getNumTwo() {
        return numTwo;
    }

    public static PairUtil minmax(Val[] arr){

        Double minMeasure = (Double) arr[0].getMeasure();
        int minInd, maxInd;
        maxInd = minInd = 0;
        Double maxMeasure = (Double) arr[0].getMeasure();
        for(int ind = 0; ind < arr.length;ind++){
            if(arr[ind].getMeasure() < minMeasure && arr[ind] != null){
                minInd = ind;
                minMeasure = arr[ind].getMeasure();
            }
            if(arr[ind].getMeasure() > maxInd && arr[ind] != null){
                maxInd = ind;
                maxMeasure = arr[ind].getMeasure();
            }
        }
        Val[] temp = new Val[2];
        temp[0] = arr[minInd];
        temp[1] = arr[maxInd];

        PairUtil retPair = new PairUtil(temp);
        return retPair;
    }

}

