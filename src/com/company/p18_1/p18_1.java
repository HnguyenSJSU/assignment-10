package com.company.p18_1;

/**
 * Hung Nguyen 013626210
 * Create an array of Val that implements Measurable
 * print out max value
 */
public class p18_1 {
    public static void main(String[] args) {
        //create a Value array of size 4
        Val[] values = new Val[4];
        values[0] = new Val<Integer>(3);
        values[1] = new Val<Double>(5.5);
        values[2] = new Val<Double>(99.0);
        values[3] = new Val<Integer>(2);
        //create a PairUtil to call minmax
        PairUtil pair = PairUtil.minmax(values);
        System.out.println(pair);
    }
}
