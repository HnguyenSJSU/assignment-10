package com.company.p18_1;

public class Val<T> implements Measurable {
    private T num;
    public Val(T temp){
        this.num = temp;
    }
    @Override
    public double getMeasure() {
        if (num instanceof Integer){
            return (Integer)(this.num);
        }
        else{
            return (Double)(this.num);
        }
    }
}
