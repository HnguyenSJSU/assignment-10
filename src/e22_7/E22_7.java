package e22_7;
import e22_7.WordCount;

/**
 Hung nguyen 013626210
 Count words in files
 */
public class E22_7 {
    static int total =0;
    public static void main(String[] args) {
        //store amount of threads needed
        int threadsCount = args.length;
        //create a list of threads
        Thread[] threadList = new Thread[args.length];
        //array of files to read
        WordCount[] wordList = new WordCount[args.length];
        try{
            //check if array is empty
            if(args.length == 0){
                throw new IllegalArgumentException();
            }
            //Append threads to list and create threads from Runnable objects
            for(int nInd = 0; nInd < threadsCount;nInd++){
                Runnable r  = new WordCount(args[nInd]);
//                Thread temp = new Thread(wordList[nInd]);
                threadList[nInd] = new Thread(r);
            }
            //start threads
            for(int nInd = 0; nInd < threadList.length;nInd++){
                threadList[nInd].start();
            }
            //join all threads
            for(int nInd = 0; nInd < threadList.length;nInd++){
                threadList[nInd].join();
            }
            //print total word counts once done
            System.out.println(String.format("Total word count: %d", total));
        }
        //catch argument errors
        catch(IllegalArgumentException e){
            System.out.println("args empty");
        }
        //catch Threads interruptions
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
