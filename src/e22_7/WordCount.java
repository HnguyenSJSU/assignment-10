package e22_7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * class to implement Runnable
 */
public class WordCount implements Runnable{
    //create value to store filename and wordcount
    private String fileName;
    private int nCount;

    /**
     * constructor for WordCount
     * @param fileText
     */
    public WordCount(String fileText){
        this.fileName = fileText;
        this.nCount = 0;
    }

    /**
     * read file and count words
     */
    @Override
    public void run() {
        try{
            //create File object and Scanner object
            File input = new File(this.fileName);
            Scanner inputFile = new Scanner(input);
            //read until end of file
            while(inputFile.hasNext()){
                //store line in a list to count words after splitting
                String[] arr = inputFile.nextLine().split(" ");
                this.nCount+= arr.length;
            }
            //print total count of words in file
            System.out.println(String.format("%s: %d", fileName,this.nCount));
            //add word counts to total amount
            E22_7.total += getnCount();
        //print error if file not found
        } catch (FileNotFoundException e) {
            System.out.println(String.format("%s cannot be opened", fileName));
        }
    }

    /**
     * get word counts
     * @return this.nCount
     */
    public int getnCount(){
        return this.nCount;
    }

}
