package P18_4;

import java.util.ArrayList;

/**
 * Class LargestArr to return maxNum
 */
public class LargestArr {
    /**
     * Method to extend Measureable and return largest number
     * @param arr
     * @param <T>
     * @return
     */
    public static <T extends Measurable> T large(ArrayList<T> arr){
        // if arr is empty return null
        if(arr.isEmpty() || arr == null){
            return null;
        }
        //Loop through array and finx max val
        T maxNum = arr.get(0);
        for (int nInd =0 ; nInd<arr.size(); nInd++){
            if (maxNum.getMeasure() < arr.get(nInd).getMeasure()){
                maxNum = arr.get(nInd);
            }
        }
        return maxNum;
    }
}
