package P18_4;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Create an arraylist of Measurable return biggest element
 */
public class p18_4 {

    public static void main(String[] args) {
        //Initialize Measureable arraylist
        ArrayList<Measurable> arrList = new ArrayList<>();
        arrList.add(new Measurable<Integer>(3));
        arrList.add(new Measurable<Integer>(6));
        arrList.add(new Measurable<Integer>(9));

        System.out.println(String.format("Largest Element: %.0f", LargestArr.large(arrList).getMeasure()));
    }
}
