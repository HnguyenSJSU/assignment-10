package P18_4;

/**
 * Create a Measureable class
 * @param <T>
 */
public class Measurable <T> {
    private T num;

    /**
     * constructor for Measureable
     * @param temp
     */
    public Measurable(T temp){
        this.num = temp;
    }

    /**
     * Return their corresponding type
     * @return
     */
    double getMeasure(){
        if (this.num instanceof Integer){
            return (Integer)(this.num);
        }
        else{
            return (Double)(this.num);
        }
    }

    /**
     * Setter for num
     * @param num
     */
    public void setNum(T num) {
        this.num = num;
    }

    /**
     * getter for num
     * @return
     */
    public T getNum() {
        return num;
    }
}
